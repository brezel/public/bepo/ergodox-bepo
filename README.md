# Layout BÉPO pour les claviers Ergodox EZ

![](https://gitlab.com/brezel/public/bepo/ergodox-bepo/raw/master/doc/bepo-qmk.png)

[💾 Télécharger la dernière version du firmware](https://gitlab.com/brezel/public/bepo/ergodox-bepo/builds/artifacts/master/download?job=qmk_firmware
)
